# Let's encrypt & NGINX in Docker container for webroot method authentication.


***
Edit:
1. data/nginx/app.conf - add domain name & ssl folder
2. init-letsencrypt.sh - add domain name & sub domain www & email
***
Start:
1. Сonfigure the server or Docker service
2. Go in u folder - cd /home
3. Clone repository - git clone ..
4. Go to folder - cd LTE_webroot
5. Set access rights Executable on script - chmod +x init-letsencrypt.sh
6. Start script - ./init-letsencrypt.sh
7. Start Docker - docker-compose up
***
Source: https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71
